const resolveConfig = require("tailwindcss/resolveConfig");
const tailwindConfig = require("./tailwind.config.js");

const fullConfig = resolveConfig(tailwindConfig);

module.exports = {
  siteMetadata: {
    title: 'Logoswift Clone',
    titleTemplate: "%s | Logo design that's as unique as your business.",
    description: 'Clone of Logoswift',
    author: '@knjo',
    gitlab: 'https://gitlab.com/KNJO/imitation-logoswift',
  },
  pathPrefix: '/imitation-logoswift',
  plugins: [
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/images/`,
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-postcss`,
      options: {
        postCssPlugins: [
          require(`tailwindcss`)(tailwindConfig),
          require(`autoprefixer`),
          ...(process.env.NODE_ENV === `production`
            ? [require(`cssnano`)]
            : []),
        ],
      },
    },
    {
      resolve: `gatsby-plugin-prefetch-google-fonts`,
      options: {
        fonts: [
          {
            family: `Neuton`,
            variants: [`300`, `400`]
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Logoswift Clone`,
        short_name: `Logoswift Clone`,
        start_url: `/`,
        background_color: fullConfig.theme.colors.orange,
        theme_color: fullConfig.theme.colors.ivory,
        display: `minimal-ui`,
        icon: `src/images/logoswift-logo.svg`,
      }
    },
    'gatsby-plugin-offline',
  ],
};
