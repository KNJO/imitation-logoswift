import React from "react";

import check from "../images/check.png";
import info from "../images/info.png";

const CardItem = ({ name, handleClick }) => (
  <div className="min-w-full inline-flex justify-between items-center">
    <div className="inline-flex items-center w-5/6 text-left">
      <span className="pr-4 flex-shrink-0">
        <img className="w-5" src={check} alt="Check icon" />
      </span>
      <p>{name}</p>
    </div>
    <span onClick={handleClick(name)}>
      <a
        href={`#${name
          .toLowerCase()
          .split(' ')
          .join('-')}`}
      >
        <img className="w-5" src={info} alt="Info icon" />
      </a>
    </span>
  </div>
);

const Card = ({ title, price, items, handleClick }) => 
    <div className="mt-8 px-8 pt-8 pb-16 bg-white rounded-lg md:w-1/2 md:m-4">
      <div className="flex justify-between items-baseline text-2xl">
        <h3 className="capitalize">{title}</h3>
        <span className="text-175 text-orange font-light tracking-wide">
          ${price}
        </span>
      </div>
      <ul className="mt-8">
        {items.map(item => {
          return (
            <li key={item} className="card-item mb-4 pb-4 border-b">
              <CardItem name={item} handleClick={handleClick} />
            </li>
          );
        })}
      </ul>
      <a href='#' className="btn w-full">Get started</a>
    </div>

export default Card;
