import React from 'react';

const Feature = ({ src, heading, text }) => (
  <div className="feature mt-8 md:w-1/3 md:inline-block md:px-4">
    <img className="inline h-20" src={src} alt="Feature" />
    <h3 className="mt-4 capitalize">{heading}</h3>
    <p className="mt-4">{text}</p>
  </div>
);

export default Feature;
