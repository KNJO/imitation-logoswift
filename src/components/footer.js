import React from 'react';

const Footer = () => (
  <footer className="text-center py-8 px-8 text-brown text-md ">
    © 2019 Logoswift. A
    {' '}
    <a href="http://clickpivot.com" className="hover:underline">
      Clickpivot
    </a>
    {' '}
    Company.
  </footer>
);

export default Footer;
