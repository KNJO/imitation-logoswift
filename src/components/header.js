import React from 'react';

import logo from '../images/logoswift-logo.svg';

const Header = () => (
  <header className="flex justify-center items-center py-8 max-w-6xl mx-auto">
    {/* <div className="py-6 my-2  w-full"> */}
    <div className="md:hidden">
      <a href="#">
        <img className="w-120 h-auto" src={logo} alt="Logoswift logo" />
      </a>
    </div>
    {/* For md screens */}
    <div className="hidden md:flex md:justify-between px-8 w-full">
      <div>
        <a href="#">
          <img className="w-120 h-auto" src={logo} alt="Logoswift logo" />
        </a>
      </div>
      <nav className="">
        <a href="#about" className="mr-10 hover:text-gray-600">
          About
        </a>
        <a href="#how-it-works" className="mr-10 hover:text-gray-600">
          How it works
        </a>
        <a href="#pricing" className="mr-10 hover:text-gray-600">
          Pricing
        </a>
        <a href="#pricing" className="hover:text-gray-600">
          Get Started
        </a>
      </nav>
    </div>
    {/* </div> */}
  </header>
);

export default Header;
