import React from 'react';

const Inspired = () => 
    <div className="antialiased">
      <div className="w-full mx-auto text-center p-4 bg-gray-800 text-white font-light text-lg">
        Inspired by{" "}
        <a className="hover:text-gray-400 underline" href="https://logoswift.com/">
          https://logoswift.com/
        </a>
      </div>
    </div>

export default Inspired;
