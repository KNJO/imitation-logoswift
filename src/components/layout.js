import React from 'react';
import PropTypes from 'prop-types';
import Img from 'gatsby-image';
import { useStaticQuery, graphql } from 'gatsby';

import Header from './header';
import Footer from './footer';
import Inspired from './inspired';

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query {
      heroMobile: file(relativePath: { eq: "logoswift-mobile.png" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid_withWebp_noBase64
          }
        }
      }
      heroDesktop: file(relativePath: { eq: "logoswift.png" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid_withWebp_noBase64
          }
        }
      }
    }
  `);

  const sources = [
    data.heroMobile.childImageSharp.fluid,
    {
      ...data.heroDesktop.childImageSharp.fluid,
      media: '(min-width: 768px)',
    },
  ];

  return (
    <div className='antialised'>
      <Inspired />
      <div className='hero bg-ivory'>
        <Header />
        <div className='mt-12 px-5 py-4 text-center md:px-8'>
          <h1 className='mx-auto mb-4 md:text-450 max-w-4xl'>
            Logo design that&apos;s as unique as your business.
          </h1>
          <br />

          <a href='#pricing' className='btn'>
            Choose your logo package
          </a>
        </div>
        <div className='mt-8'>
          <div className='w-full'>
            <Img
              alt='Working at Logoswift'
              fluid={sources}
            />
          </div>
        </div>
      </div>
      {children}
      <Footer />
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
