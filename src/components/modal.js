import React from 'react';
import close from '../images/close.png';

const Modal = ({
  children, name, selected, handleClose,
}) => (
  <div className="modal">
    <div
      className={`${
        selected === name ? 'block' : 'hidden'
      } fixed bg-black inset-0 opacity-50`}
    />
    <div
      id={name
        .toLowerCase()
        .split(' ')
        .join('-')}
      className={`${
        selected === name ? 'block' : 'hidden'
      } fixed flex justify-center items-center inset-0`}
    >
      <div className="relative w-90 max-w-lg bg-white text-grey-900 rounded">
        <button
          type="button"
          onClick={handleClose}
          className="close p-3 absolute top-0 right-0"
        >
          <img className="w-5" src={close} alt="Close icon" />
        </button>
        <div className="m-2 px-4 py-6">
          <h3 className="">{name}</h3>
          <div className="">{children}</div>
        </div>
      </div>
    </div>
  </div>
);

export default Modal;
