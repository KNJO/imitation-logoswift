import React, { useState } from 'react';
import Card from '../components/card';
import Modal from '../components/modal';

import {
  basicPackage,
  premiumPackage,
  combinedPackage,
} from '../shared/constants';

const Pricing = () => {
  const [selected, setSelected] = useState('');

  const handleClick = (name) => () => setSelected(name);
  const handleClose = () => setSelected('');

  return (
    <>
      {/* Price card */}
      <section id='pricing' className='w-full bg-ivory'>
        <div className='mx-auto'>
          <h2 className=''>Choose your logo package</h2>
          <div className='md:flex md:items-center'>
            <Card
              title='Basic package'
              price='299'
              items={basicPackage.map((item) => item.name)}
              handleClick={handleClick}
            />
            <Card
              title='Premium package'
              price='499'
              items={premiumPackage.map((item) => item.name)}
              handleClick={handleClick}
            />
          </div>
        </div>
      </section>

      {/* Modal */}
      <div className='modals'>
        <div>
          {combinedPackage.map((item, index) => {
            return (
              <div key={index}>
                <Modal
                  name={item.name}
                  selected={selected}
                  handleClose={handleClose}
                >
                  {item.info.map((info) => (
                    <p>{info}</p>
                  ))}
                </Modal>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Pricing;
