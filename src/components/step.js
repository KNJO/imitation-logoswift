import React from 'react';
import PropTypes from 'prop-types';

const Step = ({ item, heading, text }) => (
  <div className="mt-8 md:w-1/3 md:inline-block md:px-4">
    <div className="mx-auto flex items-center justify-center w-70 h-70 rounded-full border-orange border-solid border-2 text-2xl text-orange">
      {item}
    </div>
    <h3 className="mt-4 capitalize">{heading}</h3>
    <p className="mt-4">{text}</p>
  </div>
);

Step.propTypes = {
  item: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default Step;
