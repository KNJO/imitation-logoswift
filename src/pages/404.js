import React from 'react';

import SEO from '../components/seo';
import Footer from '../components/footer';

import logo from '../images/logoswift-logo.svg';

function NotFoundPage() {
  return (
    <div className="antialiased">
      <SEO title="404: Not found" />
      <header className="bg-ivory flex justify-center items-center py-8 w-full mx-auto">
        <a href="/">
          <img className="w-120 h-auto" src={logo} alt="Logoswift logo" />
        </a>
      </header>
      <main className="mt-24 h-auto flex justify-center items-center">
        <h1 className="text-orange">404: Page not found</h1>
      </main>
      <div className="fixed bottom-0 w-full bg-ivory">
        <Footer />
      </div>
    </div>
  );
}

export default NotFoundPage;
