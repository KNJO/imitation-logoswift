import React from "react";
import { graphql } from "gatsby";
import Img from "gatsby-image";

import Layout from "../components/layout";
import SEO from "../components/seo";
import Feature from "../components/feature";
import Step from "../components/step";
import Pricing from '../components/pricing'

import quality from '../images/quality.png';
import fast from '../images/fast.png';
import ethical from '../images/ethical.png';

function IndexPage({ data }) {
  return (
    <Layout>
      <SEO
        title="Logoswift Clone"
        keywords={['gatsby', 'tailwind', 'react', 'tailwindcss']}
      />
      <main>
        <section className="features md:flex md:items-start">
          <Feature
            src={quality}
            heading="Timeless quality"
            text="We don’t use AI or low-end designers to generate template branding. Our logos are custom designed around timeless archetypes, meaning you’ll receive a unique logo that will withstand the test of time."
          />
          <Feature
            src={fast}
            heading="Startup speed"
            text="Things move fast in the startup world, and we’re used to keeping up. We’ll send over your initial design concepts within 3 days and provide timely updates and revisions with each round of feedback."
          />
          <Feature
            src={ethical}
            heading="Ethical employment"
            text="Design contests run on spec work, where numerous designs are solicited but only a few designers are paid. We believe this is unethical, we pay each designer regardless of whether their design is chosen."
          />
        </section>
        <div className="md:hidden">
          <section className="w-full bg-orange text-white text-left">
            <div className="mx-auto">
              <h2 className="capitalize">Timeless logos for timeless brands</h2>
              <p className="mt-4">
                Logoswift exists to help bootstrap entrepreneurs solve the
                modern branding dilemma. You know how important visual branding
                is in today’s marketplace. You also know that as a bootstrap
                entrepreneur, you can’t afford to spend a big chunk of your
                startup budget on that branding.
              </p>
              <p className="mt-4">
                Our classical approach allows us to create fresh, unique logos
                that are as timeless as the brands they represent. Our logos are
                as comfortable in a modern setting as they would be placed 20
                years in the past, and you can count on them being just as
                comfortable 20 years in the future.
              </p>
              <p className="mt-4">
                If you’re looking for quality, timeless branding on a startup
                budget, you’re in the right place.
              </p>
            </div>
          </section>
          <div className="w-full">
            <div className="h-340 w-full">
              <Img
                fluid={data.about.childImageSharp.fluid}
                alt="Workers"
                className="h-full w-full object-center object-cover"
              />
            </div>
          </div>
        </div>
        <div id="about" className="hidden md:flex">
          <section className="w-full bg-orange text-white text-left md:p-0">
            <div className="px-8 py-20 md:ml-auto xl:w-5/6 xl:px-10">
              <h2 className="capitalize">Timeless logos for timeless brands</h2>
              <p className="mt-4">
                Logoswift exists to help bootstrap entrepreneurs solve the
                modern branding dilemma. You know how important visual branding
                is in today’s marketplace. You also know that as a bootstrap
                entrepreneur, you can’t afford to spend a big chunk of your
                startup budget on that branding.
              </p>
              <p className="mt-4">
                Our classical approach allows us to create fresh, unique logos
                that are as timeless as the brands they represent. Our logos are
                as comfortable in a modern setting as they would be placed 20
                years in the past, and you can count on them being just as
                comfortable 20 years in the future.
              </p>
              <p className="mt-4">
                If you’re looking for quality, timeless branding on a startup
                budget, you’re in the right place.
              </p>
            </div>
          </section>
          <div className="w-full">
            <div className="h-full w-full">
              <Img
                fluid={data.about.childImageSharp.fluid}
                alt="Workers"
                className="h-full w-full object-center object-cover"
              />
            </div>
          </div>
        </div>
        <section id="how-it-works" className="steps">
          <h2>How does it work?</h2>
          <div className="md:flex">
            <Step
              item={1}
              heading="Choose your package"
              text="Whether you just need a quick logo or a full startup branding package, we have the perfect option to get you up and running."
            />
            <Step
              item={2}
              heading="Introduce us to your brand"
              text="Help us get to know you, your brand, your customers, and your vision. We’ll incorporate everything you tell us into your branding."
            />
            <Step
              item={3}
              heading="Get back to business"
              text="And just like that, you can check branding off your list and get back to building your business! We’ll send you initial design concepts within 3 days."
            />
          </div>
        </section>
        <section className="hidden md:block w-full bg-orange text-white">
          <div className="w-8/12 mx-auto">
            <h2 className="">
              Logoswift is determined to design a logo for a business in your
              city. Will it be yours?
            </h2>

            <a href="#pricing" className="btn-white">
              Choose your logo package
            </a>
          </div>
        </section>
        <section className="w-full bg-ivory md:py-20">
          <h2 className="">What people are saying</h2>
          <div className="md:hidden mx-auto">
            <div className="mt-6 testimonial-photos">
              <div className="rounded-lg overflow-hidden">
                <Img
                  fluid={data.chevere.childImageSharp.fluid}
                  alt="Chevere owner"
                />
              </div>
              <div className="flex flex-row justify-between mt-6">
                <div className="rounded-lg overflow-hidden w-1/2 pr-3">
                  <Img
                    fluid={data.chevere2.childImageSharp.fluid}
                    alt="Chevere logo"
                    className="w-full h-full object-cover object-center rounded-lg"
                  />
                </div>
                <div className="rounded-lg overflow-hidden w-1/2 pl-3">
                  <Img
                    fluid={data.chevere3.childImageSharp.fluid}
                    alt="Chevere restaurant"
                    className="w-full h-full object-cover object-center rounded-lg"
                  />
                </div>
              </div>
            </div>
            <p className="mt-6 text-left text-xl">
              “Logoswift surpassed our expectations in quality and experience.
              They delivered agency quality work at an affordable price. We love
              our new logo.”
            </p>
            <div className="mt-6 text-left text-sm font-light">
              Anthony Lopez - Chévere
            </div>
          </div>
          <div className="hidden md:block mx-auto">
            <div className="md:inline-flex mt-6 testimonial-photos ">
              <div className="h-full inline-block w-1/2 rounded-lg overflow-hidden">
                <Img
                  fluid={data.chevere.childImageSharp.fluid}
                  alt="Chevere owner"
                />
              </div>
              <div className="w-1/2 inline-block h-full pl-6">
                <div className="flex flex-row justify-between mt-0">
                  <div className="rounded-lg overflow-hidden w-1/2 pr-3">
                    <Img
                      fluid={data.chevere2.childImageSharp.fluid}
                      alt="Chevere logo"
                      className="w-full h-full object-cover object-center rounded-lg"
                    />
                  </div>
                  <div className="rounded-lg overflow-hidden w-1/2 pl-3">
                    <Img
                      fluid={data.chevere3.childImageSharp.fluid}
                      alt="Chevere restaurant"
                      className="w-full h-full object-cover object-center rounded-lg"
                    />
                  </div>
                </div>
                <p className="mt-6 text-left text-2xl">
                  “Logoswift surpassed our expectations in quality and
                  experience. They delivered agency quality work at an
                  affordable price. We love our new logo.”
                </p>
                <div className="mt-6 text-left font-light text-gray-800">
                  Anthony Lopez - Chévere
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="w-full">
          <div className="mx-auto">
            <h2 className="">Some of our happy clients</h2>
            {/* Logos */}
            <div>
              {data.logos.edges.map((image, index) => 
                  <div key={index} className="logos w-1/3 md:w-1/5 h-auto inline-block p-2 md:p-8">
                    <div className="">
                      <Img
                        fluid={image.node.childImageSharp.fluid}
                        alt="Logo"
                      />
                    </div>
                  </div>
              )}
            </div>
          </div>
        </section>

        <Pricing />
      </main>
    </Layout>
  );
}
export const images = graphql`
  fragment bigImage on File {
    childImageSharp {
      fluid {
        ...GatsbyImageSharpFluid_withWebp
      }
    }
  }
`;

export const data = graphql`
  query {
    logos: allFile(sort: {order: ASC, fields: name}, filter: {name: {regex: "/logos_\\d+/"}}) {
      edges {
        node {
          id
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid_withWebp_noBase64
            }
          }
        }
      }
    }
    about: file(relativePath: {eq: "about.png"}) {
      ...bigImage
    }
    chevere: file(relativePath: {eq: "chevere.png"}) {
      ...bigImage
    }
    chevere2: file(relativePath: {eq: "chevere-2.png"}) {
      ...bigImage
    }
    chevere3: file(relativePath: {eq: "chevere-3.png"}) {
      ...bigImage
    }
  }
`;


export default IndexPage;
