const basicPackage = [
  {
    name: '2 Unique design concepts',
    info: [
      'Limiting it to 2 designs help streamline the process and allow us to deliver the best solutions.',
      'As designers, we understand our client’s target audience as well as what makes a successful logo. We know what works. Our client’s invest in our services because they trust us.',
    ],
  },
  {
    name: '2 Revisions',
    info: [
      'Once you choose one of the two design concepts, you have 2 revisions if you decide to use them.',
    ],
  },
  {
    name: 'High-res file types include PNG, EPS, SVG and PDF',
    info: [
      "You'll get PNG, EPS, SVG, and PDF logo files for all your marketing needs. Use PNGs for any digital applications and vector files to print.",
    ],
  },
  {
    name: 'Black & white, color, and transparent variations',
    info: [
      "You'll get your logo in 4 variations: Full color, colored on a transparent background, white on a transparent background and black on a transparent background.",
    ],
  },
  {
    name: 'Full ownership',
    info: [
      "Even though Logoswift designs your logo, we don't retain any rights over how you choose to use it.",
    ],
  },
];
const premiumPackage = [
  {
    name: '2 Unique design concepts',
    info: [
      'Limiting it to 2 designs help streamline the process and allow us to deliver the best solutions.',
      'As designers, we understand our client’s target audience as well as what makes a successful logo. We know what works. Our client’s invest in our services because they trust us.',
    ],
  },
  {
    name: '3 Revisions',
    info: [
      'Once you choose one of the two design concepts, you have 3 revisions if you decide to use them.',
    ],
  },
  {
    name: 'High-res file types include PNG, EPS, SVG and PDF',
    info: [
      "You'll get PNG, EPS, SVG, and PDF logo files for all your marketing needs. Use PNGs for any digital applications and vector files to print.",
    ],
  },
  {
    name: 'Black & white, color, and transparent variations',
    info: [
      "You'll get your logo in 4 variations: Full color, colored on a transparent background, white on a transparent background and black on a transparent background.",
    ],
  },
  {
    name: 'Full ownership',
    info: [
      "Even though Logoswift designs your logo, we don't retain any rights over how you choose to use it.",
    ],
  },
  {
    name: 'File storage',
    info: [
      'We save all your logo files to our Dropbox account and give you a link to access them.',
    ],
  },
  {
    name: 'Brand style guide',
    info: [
      "This document contains the rules and best practices for how to use your logo with your brand's collateral.",
      "It's a useful reference for what fonts you use, what your brand colors are, and how to present your logo on different backgrounds.",
    ],
  },
];

const combinedPackage = basicPackage.concat(
  premiumPackage.filter((p) => !basicPackage.find((b) => b.name === p.name)),
);

module.exports = {
  basicPackage,
  premiumPackage,
  combinedPackage,
};
