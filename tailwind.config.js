// See https://next.tailwindcss.com/docs/configuration for details

module.exports = {
  purge: [
    './src/**/*.js',
  ],
  theme: {
    extend: {
      colors: {
        ivory: "#fbf0e9",
        orange: "#F46445",
        brown: "#757575"
      },
      spacing: {
        "70": "70px",
        "120": "120px",
        "340": "340px",
        "90": "90%"
      },
      fontFamily: {
        heading: ["Neuton", "serif"]
      },
      fontSize: {
        "175": "1.75rem",
        "250": "2.5rem",
        "450": "4.5rem"
      }
    }
  },
  variants: {},
  plugins: []
};
